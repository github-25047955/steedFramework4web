package steed.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

import com.arjuna.ats.internal.jdbc.drivers.modifiers.list;

import steed.util.document.Cell;
import steed.util.document.ColumnWidth;
import steed.util.document.ExcelUtil;
import steed.util.document.MergedRegion;

public class ExcelTest {
	/**
	 * 用合并表格命令实体类生成复杂Excel
	 */
	@Test
	public void testMergedRegion(){
		List<List<Object>> excels = new ArrayList<>();
		List<Object> header = new ArrayList<>();
		header.add("表头1");
		header.add("表头2");
		header.add("表头3");
		header.add("表头4");
		header.add("表头5");
		excels.add(header);
		
		List<Object> row = new ArrayList<>();
		row.add("内容1");
		row.add("内容2");
		row.add("占两行");
		row.add("内容4");
		row.add("内容5");
		excels.add(row);
		List<Object> row2 = new ArrayList<>();
		row2.add("占两列");
		//因为该格被前面的单元格占了,这里可以直接添加一个空白单元格,当然,你也可以添加内容,但是内容不会和前面"占两列"的单元格合并,而是会被前面"占两列"的单元格覆盖掉
		row2.add(null);
		row2.add("内容3");
		row2.add("内容4");
		excels.add(row2);
		
		List<Object> mergedRegion = new ArrayList<>();
		//因为上面row2第一个格子占两个格,要add一个MergedRegion合并表格实体类
		mergedRegion.add(new MergedRegion(2, 2, 0, 1));
		//因为上面row的格子占两行,要add一个MergedRegion合并表格实体类
		mergedRegion.add(new MergedRegion(1, 2, 2, 2));
		excels.add(mergedRegion);
		
		try {
			ExcelUtil.createExcel(excels, "testMergedRegion", new FileOutputStream("D:\\test.xlsx"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Cell getCell(Object value){
		return new Cell(value){

			@Override
			public CellStyle getCellStyle(Workbook book) {
				CellStyle style = super.getCellStyle(book);
				style.setFillForegroundColor(HSSFColor.RED.index);  //设置填充颜色为红色
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  //设填充样式,一定要设置,不然填充颜色填不上
				
				
				return style;
			}
			
		};
	}
	
	/**
	 * 用合并表格命令实体类生成复杂Excel
	 */
	@Test
	public void testCellStyle(){
		List<List<Object>> excels = new ArrayList<>();
		List<Object> header = new ArrayList<>();
		header.add("表头1");
		header.add("表头2");
		
		header.add(getCell("自定义单元格样式"));
		header.add("宽度255*100");
		header.add("表头5");
		excels.add(header);
		
		List<Object> row = new ArrayList<>();
		row.add("内容1");
		row.add("内容2");
		row.add("内容3");
		row.add("内容4");
		row.add("内容5");
		excels.add(row);
		
		List<Object> columnWidth = new ArrayList<>();
		//因为第3列宽度为250*100,所以要添加一个列宽操作类
		columnWidth.add(new ColumnWidth(3,255*100));
		excels.add(columnWidth);
		
		try {
			ExcelUtil.createExcel(excels, "testMergedRegion", new FileOutputStream("D:\\test.xlsx"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
