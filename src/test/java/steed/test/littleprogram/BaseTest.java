package steed.test.littleprogram;

import org.junit.Test;

import steed.domain.wechat.WechatAccount;
import steed.util.base.BaseUtil;
import steed.util.wechat.MutiAccountSupportUtil;
import steed.util.wechat.WechatInterfaceInvokeUtil;
import steed.util.wechat.domain.send.RefundSend;
import steed.util.wechat.littleprogram.LittleProgramInterfaceInvokeUtil;
import steed.util.wechat.littleprogram.domain.send.TemplateMessage;

public class BaseTest {
	
	private void setWechatAccount(){
		WechatAccount account = new WechatAccount();
		account.setAppID("wx4bb5558e1efb02c7");//请自行设置你的微信小程序账号
		account.setAppSecret("cce242ab310eb9500c33cb347291a5f3");
		account.setMerchantId("1480076732");
		account.setMerchantKey("hssnifter19820815snifter19820815");
		account.setMerchantCertPath("D:\\git\\steedFramework4web\\target\\apiclient_cert.p12");
		
		MutiAccountSupportUtil.setWechatAccount(account);
	}
	@Test
	public void sendRefund(){
		setWechatAccount();
		RefundSend refundSend = new RefundSend();
		refundSend.setOp_user_id("战马");
		refundSend.setOut_trade_no("20170728000039");
		refundSend.setOut_refund_no("dd23");
		refundSend.setTotal_fee(1);
		refundSend.setRefund_fee(1);
		
		BaseUtil.outJson(WechatInterfaceInvokeUtil.refund(refundSend));
	}
	@Test
	public void testSendTemplate(){
		setWechatAccount();
		TemplateMessage templateMessage = new TemplateMessage();
		templateMessage.setTemplate_id("bdrlLDvrslV4k5Ql9meXpBsTR2_ByViV5AyLF0IwvTM");
		templateMessage.setTouser("oeJAd0bJakba0Hvy3kbrL3cNSGDU");
		templateMessage.setEmphasis_keyword("keyword1.DATA");
		templateMessage.setForm_id("wx201707281705284cc71f15860410542064");
		
		templateMessage.addData("123456789");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		templateMessage.addData("1234");
		
		LittleProgramInterfaceInvokeUtil.sendTemplateMessage(templateMessage);
	}
}
