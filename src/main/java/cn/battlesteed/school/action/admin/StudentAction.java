package cn.battlesteed.school.action.admin;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import cn.battlesteed.school.domain.Student;
import steed.action.annotation.Power;
import steed.ext.action.admin.DwzAdminAction;
@Namespace("/admin/student")
@ParentPackage(value="steed")
@InterceptorRefs({@InterceptorRef(value="mydefault")})
@Power("管理学生")
public class StudentAction extends DwzAdminAction<Student>{
	private static final long serialVersionUID = 2127023869109798260L;
	

	@Action("index")
	@Power("查看学生列表")
	public String index(){
		return super.index();
	}
	
	@Action("add")
	@Power("添加学生")
	public String add(){
		return super.add();
	}
	
	
	@Action("save")
	@Power("添加学生")
	public String save(){
		return super.save();
	}
	
	@Action("delete")
	@Power("删除学生")
	public String delete(){
		return super.delete();
	}
	@Action("edit")
	@Power("编辑学生")
	public String edit(){
		return super.edit();
	}
	@Action("update")
	@Power("编辑学生")
	public String update(){
		return super.updateNotNullField();
	}
	
}
