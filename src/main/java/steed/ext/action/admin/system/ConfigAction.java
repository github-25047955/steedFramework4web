package steed.ext.action.admin.system;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import steed.action.annotation.Power;
import steed.domain.system.Config;
import steed.ext.action.admin.DwzAdminAction;
import steed.hibernatemaster.domain.BaseDatabaseDomain;
import steed.hibernatemaster.util.DaoUtil;
import steed.hibernatemaster.util.QueryBuilder;
@ParentPackage(value="steed")
@InterceptorRefs({@InterceptorRef(value="mydefault")})
@Namespace("/admin/system/config")
@Power("系统参数管理")
public class ConfigAction extends DwzAdminAction<Config>{
	private static final long serialVersionUID = 2917184309474601008L;

	@Action("index")
	@Power("查看参数列表")
	public String index(){
		Map<String, Object> queryMap = new QueryBuilder().addNotEqual("type", 0).addNotNull("type", true).getQueryMap();
		queryMap.putAll(DaoUtil.putField2Map(getModel()));
		setRequestPage(DaoUtil.listObj(Config.class, pageSize, currentPage, queryMap, null, null));;
		//DaoUtil.createQuery(null, new StringBuffer("select * from Config c where c.type != 0 and c.type is not null")).list();
		return steed_forward;
	}
	
	@Action(value="edit")
	@Power("修改系统参数")
	public String edit(){
		Config smartGet = getModel().smartGet();
		if (smartGet.getType() == null || smartGet.getType() == 0) {
			return null;
		}
		setRequestDomain(smartGet);
		return steed_forward;
	}
	
	@Action(value="update")
	@Power("修改系统参数")
	public String update(){
		String hql = "update Config c set c.value = :value where c.kee = :kee and c.type != 0 and c.type is not null";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("kee", getModel().getKee());
		map.put("value", getModel().getValue());
		DaoUtil.beginTransaction();
		DaoUtil.createQuery(map, new StringBuffer(hql)).executeUpdate();
		
		beforeUpdate();
		writeObjectMessage(afterUpdate());
		return null;
	}
	
}
