package steed.util.document;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

import steed.hibernatemaster.domain.BaseDomain;

/**
 * excel单元格实体类
 * @author 战马
 *
 */
public class Cell extends BaseDomain{
	private static final long serialVersionUID = -8283897812039841458L;
	private Object value;
	
	public Cell() {
		super();
	}

	public Cell(Object value) {
		super();
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	/**
	 * 获取单元格样式,可重写该方法,改变单元格样式,
	 * 请自行搜索org.apache.poi.ss.usermodel.CellStyle的用法,
	 * 这里不做过多介绍
	 * 
	 * @param book
	 * @return
	 */
	public CellStyle getCellStyle(Workbook book){
		CellStyle cellStyle = book.createCellStyle();;
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION); //水平布局：居中
        cellStyle.setWrapText(true);
        return cellStyle;
	}
}
