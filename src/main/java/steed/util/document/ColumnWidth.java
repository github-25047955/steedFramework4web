package steed.util.document;

public class ColumnWidth {
	private int index;
	private int width;
	public int getIndex() {
		return index;
	}
	
	
	public ColumnWidth() {
		super();
	}


	public ColumnWidth(int index, int width) {
		super();
		this.index = index;
		this.width = width;
	}


	/**
	 * 第几列
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	public int getWidth() {
		return width;
	}
	/**
	 * 宽度
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	
}
