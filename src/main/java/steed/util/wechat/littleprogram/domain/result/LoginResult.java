package steed.util.wechat.littleprogram.domain.result;

import steed.util.wechat.domain.result.BaseWechatResult;

public class LoginResult extends BaseWechatResult{
	private String openid;
	private String unionid;
	private String session_key;
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSession_key() {
		return session_key;
	}
	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	
}
