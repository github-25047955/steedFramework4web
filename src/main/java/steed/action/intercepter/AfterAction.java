package steed.action.intercepter;

public interface AfterAction {
	public void doAfterAction();
}
